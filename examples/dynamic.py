from __future__ import annotations

from typing import TYPE_CHECKING

from yatbaf import Bot
from yatbaf import on_message
from yatbaf.di import Provide
from yatbaf.filters import Command
from yatbaf_menu import Action
from yatbaf_menu import Menu
from yatbaf_menu import Submenu
from yatbaf_menu import build_router

if TYPE_CHECKING:
    from yatbaf.types import CallbackQuery
    from yatbaf.types import Message
    from yatbaf_menu.typing import Query

SHOW = False


async def button1(q: CallbackQuery) -> None:
    await q.answer()
    await q.message.answer("click2")


async def button2(q: CallbackQuery) -> None:
    await q.answer()
    await q.message.answer("click2")


async def button2_show(q: Query) -> bool:  # noqa: U100
    return SHOW


async def switch(q: CallbackQuery, settings: Menu) -> None:
    global SHOW
    SHOW = not SHOW
    await settings.render(q)


async def switch_title(q: Query) -> str:  # noqa: U100
    if SHOW:
        return "Hide 'Click 2'"
    return "Show 'Click 2'"


@on_message(filters=[Command("menu")])
async def open_menu(message: Message, main: Menu) -> None:
    await main.render(message)


def create_menu() -> Menu:
    return Menu(
        title="Menu title",
        name="main",
        buttons=[
            [
                Action(title="Click 1", action=button1),
                Action(title="Click 2", action=button2, show=button2_show),
            ],
            Submenu(title="Settings", menu="settings"),
        ],
        submenu=[
            Menu(
                title="Settigns",
                name="settings",
                buttons=[Action(title=switch_title, action=switch)],
                back_btn_title="Back",
            ),
        ],
    )


if __name__ == "__main__":
    import os

    from yatbaf.exceptions import InvalidTokenError

    main = create_menu()
    settings = main.get_submenu("settings")
    try:
        Bot(
            token=os.environ.get("BOT_TOKEN", ""),
            handlers=[open_menu, build_router(main)],
            dependencies={
                "main": Provide(main.provide),
                "settings": Provide(settings.provide),
            },
        ).run()
    except InvalidTokenError:
        print("token missing")
