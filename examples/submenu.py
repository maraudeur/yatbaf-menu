from __future__ import annotations

from typing import TYPE_CHECKING

from yatbaf import Bot
from yatbaf import on_message
from yatbaf.di import Provide
from yatbaf.filters import Command
from yatbaf_menu import Action
from yatbaf_menu import Menu
from yatbaf_menu import Submenu
from yatbaf_menu import build_router

if TYPE_CHECKING:
    from yatbaf.types import Message
    from yatbaf_menu.typing import Query


async def button1(q: Query) -> None:
    await q.answer()
    await q.message.answer("click1")


async def button2(q: Query) -> None:
    await q.answer()
    await q.message.answer("click2")


@on_message(filters=[Command("menu")])
async def open_menu(message: Message, menu: Menu) -> None:
    await menu.render(message)


def create_menu() -> Menu:
    submenu = Menu(
        title="Submenu title",
        name="click",
        buttons=[
            [
                Action(title="Click 1", action=button1),
                Action(title="Click 2", action=button2),
            ],
        ],
        back_btn_title="Back",
    )

    main = Menu(
        title="Menu title",
        name="main",
        buttons=[Submenu(title="Submenu", menu=submenu.name)],
        submenu=[submenu],
    )

    return main


if __name__ == "__main__":
    import os

    from yatbaf.exceptions import InvalidTokenError

    menu = create_menu()
    try:
        Bot(
            token=os.environ.get("BOT_TOKEN", ""),
            handlers=[open_menu, build_router(menu)],
            dependencies={
                "menu": Provide(menu.provide),
            },
        ).run()
    except InvalidTokenError:
        print("token missing")
