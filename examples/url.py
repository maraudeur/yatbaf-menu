from __future__ import annotations

from typing import TYPE_CHECKING

from yatbaf import Bot
from yatbaf import on_message
from yatbaf.di import Provide
from yatbaf.filters import Command
from yatbaf_menu import URL
from yatbaf_menu import Menu
from yatbaf_menu import build_router

if TYPE_CHECKING:
    from yatbaf.types import Message


@on_message(filters=[Command("menu")])
async def open_menu(message: Message, menu: Menu) -> None:
    await menu.render(message)


def create_menu() -> Menu:
    return Menu(
        title="Menu title",
        buttons=[
            [
                URL(
                    title="yatbaf",
                    url="https://codeberg.org/maraudeur/yatbaf",
                ),
                URL(
                    title="yatbaf-menu",
                    url="https://codeberg.org/maraudeur/yatbaf-menu",
                ),
            ],
        ],
    )


if __name__ == "__main__":
    import os

    from yatbaf.exceptions import InvalidTokenError

    menu = create_menu()
    try:
        Bot(
            token=os.environ.get("BOT_TOKEN", ""),
            handlers=[open_menu, build_router(menu)],
            dependencies={
                "menu": Provide(menu.provide),
            },
        ).run()
    except InvalidTokenError:
        print("token missing")
