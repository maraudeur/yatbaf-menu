from __future__ import annotations

from typing import TYPE_CHECKING

from yatbaf import Bot
from yatbaf import on_message
from yatbaf.di import Provide
from yatbaf.filters import Command
from yatbaf_menu import Choice
from yatbaf_menu import Menu
from yatbaf_menu import build_router

if TYPE_CHECKING:
    from yatbaf.types import CallbackQuery
    from yatbaf.types import Message


async def button1(q: CallbackQuery) -> None:
    await q.answer()
    await q.message.answer(q.data)


@on_message(filters=[Command("menu")])
async def open_menu(message: Message, menu: Menu) -> None:
    await menu.render(message)


def create_menu() -> Menu:
    return Menu(
        title="Menu title",
        buttons=[
            Choice(
                action=button1,
                items=[1, 2, 3],
                items_per_row=3,
            ),
        ],
    )


if __name__ == "__main__":
    import os

    from yatbaf.exceptions import InvalidTokenError

    menu = create_menu()
    try:
        Bot(
            token=os.environ.get("BOT_TOKEN", None),
            handlers=[open_menu, build_router(menu)],
            dependencies={
                "menu": Provide(menu.provide),
            },
        ).run()
    except InvalidTokenError:
        print("token missing")
