from __future__ import annotations

from itertools import islice
from typing import TYPE_CHECKING

from yatbaf import Bot
from yatbaf import on_message
from yatbaf.di import Provide
from yatbaf.filters import Command
from yatbaf_menu import Menu
from yatbaf_menu import Paging
from yatbaf_menu import build_router

if TYPE_CHECKING:
    from collections.abc import Iterable

    from yatbaf.types import CallbackQuery
    from yatbaf.types import Message
    from yatbaf_menu.typing import Query


async def get_items(
    q: Query,  # noqa: U100
    offset: int,
    limit: int,
) -> tuple[Iterable[tuple[str, str]], bool]:
    """
    :param q: Current CallbackQuery or Message.
    :param offset: Offset for the current batch.
    :param limit: Number of items in a batch.
    """
    size = 12
    last_item = offset + limit
    return (
        ((str(i), str(i)) for i in islice(range(size), offset, last_item)),
        last_item >= size,
    )


async def button1(q: CallbackQuery) -> None:
    await q.answer()
    await q.message.answer(q.data)


@on_message(filters=[Command("menu")])
async def open_menu(message: Message, menu: Menu) -> None:
    await menu.render(message)


def create_menu() -> Menu:
    return Menu(
        title="Menu title",
        buttons=[
            Paging(
                action=button1,
                items=get_items,
                items_per_row=4,
                items_per_page=4,
            ),
        ],
    )


if __name__ == "__main__":
    import os

    from yatbaf.exceptions import InvalidTokenError

    menu = create_menu()
    try:
        Bot(
            token=os.environ.get("BOT_TOKEN", ""),
            handlers=[open_menu, build_router(menu)],
            dependencies={
                "menu": Provide(menu.provide),
            },
        ).run()
    except InvalidTokenError:
        print("token missing")
